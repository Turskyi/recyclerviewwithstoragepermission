package com.turskyi.recyclerview.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.turskyi.recyclerview.DetailActivity
import com.turskyi.recyclerview.room.models.Picture

class RecyclerAdapter(val aContext: Context, val aPictureList: Array<Picture>) : RecyclerView.Adapter<RecyclerAdapter.FolderViewHolder>() {

    /* this method is returning the view for each item in the list */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FolderViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            com.turskyi.recyclerview.R.layout.list_item,
            parent, false
        )
        return FolderViewHolder(view)
    }

    /* this method is binding the data on the list */
    override fun onBindViewHolder(holder: FolderViewHolder, position: Int) {
        holder.bindView(aPictureList[position],aContext)
    }
    /* this method is giving the size of the list */
        override fun getItemCount()= aPictureList.size

    /* the class is holding the list item view */
    class FolderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val aFolder = itemView.findViewById(com.turskyi.recyclerview.R.id.ivPicture) as ImageView
            fun bindView(picture: Picture, aContext: Context) {

                /* this line create a different pictures */
                aFolder.setImageDrawable(aContext.getDrawable(picture.path))

                aFolder.setOnClickListener {
                    val anIntent = Intent(aContext, DetailActivity::class.java)
                    anIntent.putExtra("Image", picture.path)
                    aContext.startActivity(anIntent)
                }
            }
        }
    }



