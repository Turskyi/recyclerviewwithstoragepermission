package com.turskyi.recyclerview

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.turskyi.recyclerview.adapter.RecyclerAdapter
import com.turskyi.recyclerview.room.AppDataBase
import com.turskyi.recyclerview.room.DBAppThread
import com.turskyi.recyclerview.room.models.Picture
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

/** Recycler Grid View with incrementing of number of columns */
class MainActivity : AppCompatActivity() {

    companion object {
        const val PERMISSION_READ_STATE = 10001
    }

    private var pictures = ArrayList<Picture>()

    private lateinit var aDBThread: DBAppThread
    private var aDB: AppDataBase? = null
    private val aUIHandler = Handler()

    private lateinit var aRandomList: Array<Picture>

    private var quantityOfColumns: Int = 1

    private lateinit var aRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) getPermission()

        pictures.add(Picture(0, R.drawable.any_language))
        pictures.add(Picture(0, R.drawable.balcony))
        pictures.add(Picture(0, R.drawable.cabinet))
        pictures.add(Picture(0, R.drawable.edward_hopper_rooms_by_the_sea))
        pictures.add(Picture(0, R.drawable.gun))
        pictures.add(Picture(0, R.drawable.projector))

        aDBThread = DBAppThread("MainActivity")
        aDBThread.start()
        aDB = AppDataBase.getInstance(this)

        increment_of_columns.setOnClickListener {
            incrementOfColumns()
        }

        decrement_of_columns.setOnClickListener {
            decrementOfColumns()
        }

        aRecyclerView = findViewById(R.id.rv)
        getNumberOfColumns()

        aRandomList = Array(30) { pictures.random() }

        getAllLivePictures()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_READ_STATE -> {
                /* If request is cancelled, the result arrays are empty. */
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(this, "permission allowed", Toast.LENGTH_SHORT).show()
                } else {
                    restart()
                }
                return
            }
        }
    }

    private fun getAllLivePictures() {
        aDB?.folderDAO()?.getLiveAll()?.observe(this,
            Observer<List<Picture>> { listOfFolders ->
                listOfFolders?.let {
                    initAdapter(list = this.aRandomList)
                }
            })
    }


    private fun initAdapter(list: Array<Picture>) {
        aUIHandler.post {
            rv.adapter = RecyclerAdapter(aContext = this, aPictureList = list)
        }
    }

    private fun getNumberOfColumns() {
        val aGridLayoutManager = GridLayoutManager(this@MainActivity, quantityOfColumns)
        aRecyclerView.layoutManager = aGridLayoutManager
    }

    /**
     * This method is called when the plus button is clicked.
     */
    private fun incrementOfColumns() {
        if (quantityOfColumns == 10) {
            return
        }
        quantityOfColumns += 1
        displayQuantityOfColumns(quantityOfColumns)
    }

    /**
     * This method is called when the minus button is clicked.
     */
    private fun decrementOfColumns() {
        if (quantityOfColumns == 1) {
            return
        }
        quantityOfColumns -= 1
        displayQuantityOfColumns(quantityOfColumns)
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private fun displayQuantityOfColumns(quantityOfColumns: Int) {
        quantity_text_view.text = "$quantityOfColumns"
        getNumberOfColumns()
    }

    private fun restart() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun getPermission() {
        ActivityCompat.requestPermissions(
            this,
            listOf(Manifest.permission.WRITE_EXTERNAL_STORAGE).toTypedArray(),
            PERMISSION_READ_STATE
        )
    }

    override fun onDestroy() {
        AppDataBase.destroyInstance()
        super.onDestroy()
    }
}
