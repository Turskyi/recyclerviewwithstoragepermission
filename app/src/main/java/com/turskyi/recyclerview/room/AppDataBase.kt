package com.turskyi.recyclerview.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.turskyi.recyclerview.room.dao.FolderDAO
import com.turskyi.recyclerview.room.models.Picture

@Database(entities = [Picture::class], version = 1, exportSchema = true)

abstract class AppDataBase : RoomDatabase() {

    abstract fun folderDAO(): FolderDAO

    companion object {

        private var INSTANCE: AppDataBase? = null

        fun getInstance(context: Context): AppDataBase? {
            if (INSTANCE == null) {
                synchronized(AppDataBase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDataBase::class.java,
                        "folders.db"
                    ).build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}