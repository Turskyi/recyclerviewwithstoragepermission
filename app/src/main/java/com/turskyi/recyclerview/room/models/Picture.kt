package com.turskyi.recyclerview.room.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = Picture.TABLE_NAME)
data class Picture(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = COLUMN_ID) var id: Int,
    @ColumnInfo(name = COLUMN_NAME) var path: Int
){
    companion object{
        const val TABLE_NAME = "Images"
        const val COLUMN_ID = "id"
        const val COLUMN_NAME = "image_path"
    }
}