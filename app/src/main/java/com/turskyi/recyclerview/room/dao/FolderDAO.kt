package com.turskyi.recyclerview.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.turskyi.recyclerview.room.models.Picture

@Dao
interface FolderDAO {

    @Query("SELECT * FROM ${Picture.TABLE_NAME}")
    fun getAll(): List<Picture>

    @Query("SELECT * FROM ${Picture.TABLE_NAME}")
    fun getLiveAll(): LiveData<List<Picture>>

    @Insert
    fun insert(picture: Picture)

    @Query("DELETE FROM ${Picture.TABLE_NAME}")
    fun deleteAll()

    @Query("DELETE FROM ${Picture.TABLE_NAME} WHERE ${Picture.COLUMN_ID} = :id")
    fun deleteById(id: Int)

    @Update
    fun update(picture: Picture)

    @Delete
    fun deleteFolder(picture: Picture)

}