package com.turskyi.recyclerview.room

import android.os.Handler
import android.os.HandlerThread

class DBAppThread(threadName: String) : HandlerThread(threadName) {

    private lateinit var aPersonalHandler: Handler

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        aPersonalHandler = Handler(looper)
    }
}